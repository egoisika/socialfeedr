<?php
if (!isset($_SESSION)) { session_start(); }
require_once( '/../private/Facebook/GraphObject.php' );
require_once( '/../private/Facebook/GraphSessionInfo.php' );
require_once( '/../private/Facebook/GraphUser.php' );
require_once( '/../private/Facebook/FacebookSession.php' );
require_once( '/../private/Facebook/HttpClients/FacebookCurl.php' );
require_once( '/../private/Facebook/HttpClients/FacebookHttpable.php' );
require_once( '/../private/Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( '/../private/Facebook/FacebookResponse.php' );
require_once( '/../private/Facebook/FacebookSDKException.php' );
require_once( '/../private/Facebook/FacebookRequestException.php' );
require_once( '/../private/Facebook/FacebookAuthorizationException.php' );
require_once( '/../private/Facebook/FacebookRequest.php' );
require_once( '/../private/Facebook/FacebookRedirectLoginHelper.php' );
require_once( '/../private/Facebook/Entities/AccessToken.php' );
require_once( '/../private/Facebook/Entities/SignedRequest.php' );
require_once( '/../private/twitteroauth-master/config.php' );
// session_start();
require_once('/../private/twitteroauth-master/twitteroauth/twitteroauth.php');
require_once('/../private/twitteroauth-master/config.php');
require_once('/../private/socialFeedr.php'); 
require_once('/../private/Instagram-PHP-API-master/instagram.class.php'); 

use Facebook\GraphSessionInfo;
use Facebook\GraphUser;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpable;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;
// use Instagram\Instagram;

	// Replace the APP_ID and APP_SECRET with your apps credentials
FacebookSession::setDefaultApplication( FB_KEY, FB_SECRET );
// initialize class
$instagram = new Instagram(array(
  'apiKey'      => INSTA_KEY,
  'apiSecret'   => INSTA_SECRET,
  'apiCallback' => REDIRECT_URI_INSTA // must point to success.php
));
$instagramLoginUrl = $instagram->getLoginUrl();

$helper = new FacebookRedirectLoginHelper( REDIRECT_URI );
 
// Check if existing session exists
if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
 
  // Create new session from saved access_token
  $session = new FacebookSession( $_SESSION['fb_token'] );
 
  // Validate the access_token to make sure it's still valid
  try {
    if ( ! $session->validate() ) {
      $session = null;
    }
  } catch ( Exception $e ) {
    // Catch any exceptions
    $session = null;
  }
} else {
 
  // No session exists
  try {
    $session = $helper->getSessionFromRedirect();
  } catch( FacebookRequestException $ex ) {
 
    // When Facebook returns an error
  } catch( Exception $ex ) {
 
    // When validation fails or other local issues
    echo $ex->message;
  }
}
 
// Check if a session exists
if ( isset( $session ) ) {
 
  // Save the session
  $_SESSION['fb_token'] = $session->getToken();
 
  // Create session using saved token or the new one we generated at login
  $session = new FacebookSession( $session->getToken() );
 
  // Create the logout URL (logout page should destroy the session)
  $logoutURL = $helper->getLogoutUrl( $session, 'http://socialfeedr.dev.lan/clearsessions.php' );
} else {
  // No session
 
  // Requested permissions - optional
  $permissions = array(
    'email',
    'user_friends',
    'public_profile'
  );
 
  // Get login URL
  $loginUrl = $helper->getLoginUrl($permissions);
}
	// $socialFeedr= new SocialFeedr();
	?>
<!DOCTYPE html>
<html>
<head> 
<link rel="stylesheet" type="text/css" href="/../css/bootstrap.css">
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</head>
<body>
<article class="well">
<header>
<nav class="bs-component"><ul class="nav nav-pills">

	<li>
		<a href="<?php echo $instagramLoginUrl;?>" title="Instagram" class="instaLoggedIn">Login with Instagram</a>
	</li>

	<?php
	 if(isset($loginUrl)):
	 	?>
	<li>
		<a href="<?php echo $loginUrl;?>" title="Facebook" class="facebookLoggedIn"> Login with Facebook</a>
	</li>
<?php
 endif;
 ?>
	<?php if(!isset($_SESSION['status'])):?>
	<li>
		<a href="redirect.php" title="Twitter" class="twitterLoggedIn"> Login with Twitter</a>
	</li>
	<?php endif;?>
	<li>
		<a href="clearsessions.php" title="Twitter" > Logout from all</a>
	</li>
</ul></nav>
</header>
<nav class="bs-component"><ul class="nav nav-pills">
	<li>
		<a href="#" title="Instagram" class="instaLog">Instagram</a>
	</li>
	<li>
		<a href="#" title="Facebook" class="facebookLog">Facebook</a>
	</li>
	<li>
		<a href="#" title="Twitter" class="twitterLog">Twitter</a>
	</li>
</ul></nav>
<section class="twitter">
<h2>Twitter Feed</h2>
<?php 
	// $socialFeedr= new SocialFeedr();
	// // printf(format)
	// // if (!isset($_SESSION)) { 
	// 	$socialFeedr->login(json_encode('twitter'));
	// // }
if(isset($_SESSION['status']))
{
	if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
	    header('Location: ./clearsessions.php');
	}
	$access_token = $_SESSION['access_token'];
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
	$content = $connection->get('account/verify_credentials');
	$twitteruser = $content->{'screen_name'};
	$notweets = 10;
	$tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitteruser."&count=".$notweets);

	print " <strong>Name :</strong> ".$content->{'name'}; 
	echo "<br/>";
	$name = $content->{'name'}; 
	print "<strong>Screen name :</strong> ".$content->{'screen_name'};
	$screen_name = $content->{'screen_name'};
	echo "<br/>";
	print "<strong>User id : </strong> ".$content->{'id'}; 

	$img_link = $content->{'profile_image_url'};
	$id = $content->{'id'}; 

	echo "<br/>";
	print "Location : ".$content->{'location'}; 
	$location = $content->{'location'}; 
	echo "<br/>";
	$date = date("Ymd"); 
	echo "<p><img src=\"$img_link\"></p>";
	echo "<b>Latest 10 tweets:</b> <br/>";
	foreach ($tweets as $item)
	{
		echo "<p>".$item->text."</p>";
		
	}
}
	
?>
</section>
<section class="insta">
<h2>Instagram Feed</h2>
	<?php 
	// var_dump($_SESSION['instagramData']); exit(); ?>
</section>
<section class="facebook">
<h2>Facebook Feed</h2>
<?php if($session){
	// $socialfeedr->displayAllFB($session);
}
$request = (new FacebookRequest( $session, 'GET', '/me' ))->execute();
$statuses = (new FacebookRequest( $session, 'GET', '/me/picture?type=large&redirect=false' ))->execute();
// Get response as an array
$picture = $statuses->getGraphObject()->asArray();
echo "<img src=\"$picture->url\">";
$profile = $request->getGraphObject()->asArray();
foreach ($profile as $key =>$val)
{
	if($key!='link')
	{
		echo "<p><strong>$key</strong> :$val</p>";
	}
	else
	{
		echo "<p><strong>$key</strong>:<a href=\"$val\">Link to Profile</a></p>";
	}
	
}
// echo $status;

// echo $profile->getId();
// echo $profile->getName();
// var_dump($picture);
?>
</section>
<footer>
	
</footer>
</article>
</body>
</html>